import 'package:json_annotation/json_annotation.dart';

part 'AppDebugApiBean.g.dart';

///模板
@JsonSerializable()
class AppDebugApiBean {
  String interface;
  String parameter;
  String apiData;
  String time;
  List<AppDebugApiBean> children;

  AppDebugApiBean({
    this.interface,
    this.parameter,
    this.apiData,
    this.time,
    this.children,
  });

  factory AppDebugApiBean.fromJson(Map<String, dynamic> json) =>
      _$AppDebugApiBeanFromJson(json);

  Map<String, dynamic> toJson() => _$AppDebugApiBeanToJson(this);
}
