// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'AppDebugApiBean.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AppDebugApiBean _$AppDebugApiBeanFromJson(Map<String, dynamic> json) {
  return AppDebugApiBean(
    interface: json['interface'] as String,
    parameter: json['parameter'] as String,
    apiData: json['apiData'] as String,
    time: json['time'] as String,
    children: (json['children'] as List)
        ?.map((e) => e == null
            ? null
            : AppDebugApiBean.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$AppDebugApiBeanToJson(AppDebugApiBean instance) =>
    <String, dynamic>{
      'interface': instance.interface,
      'parameter': instance.parameter,
      'apiData': instance.apiData,
      'time': instance.time,
      'children': instance.children,
    };
