import 'package:flutter/cupertino.dart';
import 'package:flutter_base/view/bs_view.dart';

///多状态布局
///使用模板查看[TestManyStateView]
///by grus95
abstract class BsManyStateView extends BsView<BsManyStateViewVM> {
  final ManyStateType manyStateType;
  final Widget child;
  final GestureTapCallback onTap;

  BsManyStateView({
    Key key,
    @required this.manyStateType,
    this.onTap,
    this.child,
  }) : super(key: key);

  @override
  Widget buildScaffold(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      child: GestureDetector(
        onTap: onTap,
        child: _checkTypeView(
          context,
          manyStateType,
        ),
      ),
    );
  }

  Widget _onLoadingPageView(BuildContext context) {
    return child ??
        (dfLoadingPageView(context) ?? CupertinoActivityIndicator());
  }

  ///默认加载布局-page
  Widget dfLoadingPageView(BuildContext context);

  Widget _onLoadingMiniView(BuildContext context) {
    return child ??
        (dfLoadingMiniView(context) ?? CupertinoActivityIndicator());
  }

  ///默认加载布局-mini
  Widget dfLoadingMiniView(BuildContext context);

  Widget _onErrorPageView(BuildContext context) {
    return child ?? dfErrorPageView(context);
  }

  ///默认异常布局-page
  Widget dfErrorPageView(BuildContext context);

  Widget _onErrorMiniView(BuildContext context) {
    return child ?? dfErrorMinView(context);
  }

  ///默认异常布局-mini
  Widget dfErrorMinView(BuildContext context);

  Widget _onEmptyPageView(BuildContext context) {
    return child ?? dfEmptyPageView(context);
  }

  ///默认空数据布局-page
  Widget dfEmptyPageView(BuildContext context);

  Widget _onEmptyMiniView(BuildContext context) {
    return child ?? dfEmptyMiniView(context);
  }

  ///默认空数据布局
  Widget dfEmptyMiniView(BuildContext context);

  ///判断类型布局
  Widget _checkTypeView(BuildContext context, ManyStateType state) {
    Widget view;
    switch (state) {
      case ManyStateType.normal:
        view = Container();
        break;
      case ManyStateType.loading_page:
        view = _onLoadingPageView(context);
        break;
      case ManyStateType.error_page:
        view = _onErrorPageView(context);
        break;
      case ManyStateType.empty_page:
        view = _onEmptyPageView(context);
        break;
      case ManyStateType.loading_mini:
        view = _onLoadingMiniView(context);
        break;
      case ManyStateType.error_mini:
        view = _onErrorMiniView(context);
        break;
      case ManyStateType.empty_mini:
        view = _onEmptyMiniView(context);
        break;
    }

    return SingleChildScrollView(child: view);
  }

  @override
  BsManyStateViewVM buildViewModel(BuildContext context) {
    return BsManyStateViewVM();
  }
}

///数据逻辑
class BsManyStateViewVM extends BsViewModel {
  @override
  void buildCompleted(BuildContext context) {}

  @override
  void disposeVM() {}

  @override
  void initVM(BuildContext context) {}
}

enum ManyStateType {
  ///正常
  normal,

  ///加载-page
  loading_page,

  ///异常-page
  error_page,

  ///空数据-page
  empty_page,

  ///加载-mini
  loading_mini,

  ///异常-mini
  error_mini,

  ///空数据-mini
  empty_mini,
}
