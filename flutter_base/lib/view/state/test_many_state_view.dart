import 'package:flutter/material.dart';
import 'package:flutter_base/view/state/bs_many_state_view.dart';

class TestManyStateView extends BsManyStateView {
  final ManyStateType manyStateType;
  final Widget child;
  final GestureTapCallback onTap;

  ///加载状态-一般在page使用
  TestManyStateView.loadingPage({
    this.onTap,
  })  : this.manyStateType = ManyStateType.loading_page,
        this.child = null;

  ///异常状态-一般在page使用
  TestManyStateView.errorPage({
    this.onTap,
  })  : this.manyStateType = ManyStateType.error_page,
        this.child = null;

  ///空数据状态-一般在page使用
  TestManyStateView.emptyPage({
    this.onTap,
  })  : this.manyStateType = ManyStateType.empty_page,
        this.child = null;

  ///加载状态-一般在小布局上使用
  TestManyStateView.loadingMini({
    this.onTap,
  })  : this.manyStateType = ManyStateType.loading_mini,
        this.child = null;

  ///异常状态-一般在小布局上使用
  TestManyStateView.errorMini({
    this.onTap,
  })  : this.manyStateType = ManyStateType.error_mini,
        this.child = null;

  ///空数据状态-一般在小布局上使用
  TestManyStateView.emptyMini({
    this.onTap,
  })  : this.manyStateType = ManyStateType.empty_mini,
        this.child = null;

  @override
  Widget dfEmptyMiniView(BuildContext context) {
    throw UnimplementedError();
  }

  @override
  Widget dfEmptyPageView(BuildContext context) {
    throw UnimplementedError();
  }

  @override
  Widget dfErrorMinView(BuildContext context) {
    throw UnimplementedError();
  }

  @override
  Widget dfErrorPageView(BuildContext context) {
    throw UnimplementedError();
  }

  @override
  Widget dfLoadingMiniView(BuildContext context) {
    throw UnimplementedError();
  }

  @override
  Widget dfLoadingPageView(BuildContext context) {
    throw UnimplementedError();
  }
}
