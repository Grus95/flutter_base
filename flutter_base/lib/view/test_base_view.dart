import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_base/flutter_base.dart';
import 'package:flutter_custom_dialog/flutter_custom_dialog.dart';

export 'package:fixnum/fixnum.dart';
export 'package:flutter/material.dart';
export 'package:provider/single_child_widget.dart';
export 'package:provider/provider.dart';

/*

///数据逻辑
///by grus95
class TestVM extends BaseViewModel{

  @override
  void initVM(BuildContext context) {

  }

  @override
  void buildCompleted(BuildContext context) {

  }

  @override
  void disposeVM() {

  }

}


///UI界面
///by grus95
class TestPage extends BaseView<TestVM>{
  @override
  Widget buildScaffold(BuildContext context) {
    return viewModelCS(
      (ct, vm, cd) => Scaffold(
        appBar: _onPageAppBarView(ct, vm),
        body: _onPageBodyView(ct, vm),
      ),
    );
  }

  ///导航栏
  PreferredSizeWidget _onPageAppBarView(BuildContext context, TestVM vm) {
    return AutoAppBarView(
      titleName: '界面标题',
    );
  }

  ///内容
  Widget _onPageBodyView(BuildContext context, TestVM vm) {
    return Container();
  }

  @override
  TestVM buildViewModel(BuildContext context) {
    return TestVM();
  }

}

*/

///基类
abstract class BaseView<T extends BaseViewModel> extends BsView<T> {}

///所有viewModel的父类，提供一些公共功能
abstract class BaseViewModel extends BsViewModel {
  ///布局内网络请求主线程的token
  CancelToken _mCancelToken;

  CancelToken get mCancelToken => _mCancelToken;

  @override
  void init(BuildContext context) {
    _init(context);
    super.init(context);
  }

  ///内部初始化
  void _init(BuildContext context) {
    _mCancelToken = CancelToken();
    YYDialog.init(context);
    // initEventBus(context);
  }

  @override
  void disposeAll() {
    _dispose();
    super.disposeAll();
  }

  ///内部回收
  void _dispose() {
    _mCancelToken?.cancel();
    // disposeEventBus();
  }
}
