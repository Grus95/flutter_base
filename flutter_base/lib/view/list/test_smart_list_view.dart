import 'package:flutter/material.dart';
import 'package:flutter_base/view/list/bs_smart_list_view.dart';
import 'package:flutter_easyrefresh/easy_refresh.dart';

class SmartListView extends BsSmartListView {
  ///布局类型
  final SmartType smartType;

  /// Header
  final Header header;

  /// Footer
  final Footer footer;

  ///默认值[BsSmartBean]
  final BsSmartBean dfSmartBean;

  ///是否显示首次全局刷新布局[firstRefreshWidget]
  ///是否显示全局异常布局[errorWidget]
  ///是否显示全局空数据布局[emptyWidget]
  final bool isShowFullState;

  ///第一次刷新布局
  final OnLoadingDataView firstRefreshWidget;

  ///网络错误布局
  final OnErrorDataView errorWidget;

  ///网络错误提示语
  final String errorTipText;

  ///空数据布局
  final OnEmptyDataView emptyWidget;

  ///空数据提示语
  final String emptyTipText;

  ///刷新加载控制器
  final EasyRefreshController refreshController;

  ///滚动控制器
  final ScrollController scrollController;

  /// 顶部回弹(Header的overScroll属性优先，且onRefresh和header都为null时生效)
  final bool topBouncing;

  /// 底部回弹(Footer的overScroll属性优先，且onLoad和footer都为null时生效)
  final bool bottomBouncing;

  ///新增数据List回调
  final OnAddSmartBeanListCallback smartBeanListCallback;

  ///数据绑定布局回调[SmartType.single]
  final OnSingle2View single2view;

  ///数据绑定布局回调[SmartType.item]
  final OnItem2View item2view;

  ///数据绑定布局回调[SmartType.slivers]
  final OnSlivers2View slivers2View;

  ///数据绑定布局回调[SmartType.builder]
  final OnBuilder2View builder2view;

  ///单布局
  SmartListView.single({
    this.header,
    this.footer,
    this.dfSmartBean,
    this.isShowFullState,
    this.firstRefreshWidget,
    this.errorWidget,
    this.errorTipText,
    this.emptyWidget,
    this.emptyTipText,
    this.refreshController,
    this.scrollController,
    this.topBouncing,
    this.bottomBouncing,
    @required this.smartBeanListCallback,
    @required this.single2view,
  })  : this.smartType = SmartType.single,
        this.item2view = null,
        this.slivers2View = null,
        this.builder2view = null;

  ///列表布局
  SmartListView.item({
    this.header,
    this.footer,
    this.dfSmartBean,
    this.isShowFullState,
    this.firstRefreshWidget,
    this.errorWidget,
    this.errorTipText,
    this.emptyWidget,
    this.emptyTipText,
    this.refreshController,
    this.scrollController,
    this.topBouncing,
    this.bottomBouncing,
    @required this.smartBeanListCallback,
    @required this.item2view,
  })  : this.smartType = SmartType.item,
        this.single2view = null,
        this.slivers2View = null,
        this.builder2view = null;

  ///slivers布局
  SmartListView.slivers({
    this.header,
    this.footer,
    this.dfSmartBean,
    this.isShowFullState,
    this.firstRefreshWidget,
    this.errorWidget,
    this.errorTipText,
    this.emptyWidget,
    this.emptyTipText,
    this.refreshController,
    this.scrollController,
    this.topBouncing,
    this.bottomBouncing,
    @required this.smartBeanListCallback,
    @required this.slivers2View,
  })  : this.smartType = SmartType.slivers,
        this.single2view = null,
        this.item2view = null,
        this.builder2view = null;

  ///自定义布局
  SmartListView.builder({
    this.header,
    this.footer,
    this.dfSmartBean,
    this.isShowFullState,
    this.firstRefreshWidget,
    this.errorWidget,
    this.errorTipText,
    this.emptyWidget,
    this.emptyTipText,
    this.refreshController,
    this.scrollController,
    this.topBouncing,
    this.bottomBouncing,
    @required this.smartBeanListCallback,
    @required this.builder2view,
  })  : this.smartType = SmartType.builder,
        this.single2view = null,
        this.item2view = null,
        this.slivers2View = null;

  @override
  Widget dfLoadingDataView(BuildContext context, BsSmartListVM vm) {
    return Container(child: Text('显示加载中布局'));
  }

  @override
  Widget dfEmptyDataView(BuildContext context, BsSmartListVM vm) {
    return Container(child: Text('显示空数据布局'));
  }

  @override
  Widget dfErrorDataView(BuildContext context, BsSmartListVM vm) {
    return Container(child: Text('显示异常布局'));
  }

  @override
  Header dfHeader(BuildContext context) {
    return ClassicalHeader(
      enableInfiniteRefresh: false,
      // refreshText: languageCT(context).mSmartPullToRefreshText,
      // refreshReadyText: languageCT(context).mSmartReleaseToRefreshText,
      // refreshingText: languageCT(context).mSmartRefreshingText,
      // refreshedText: languageCT(context).mSmartRefreshedText,
      // refreshFailedText: languageCT(context).mSmartRefreshFailedText,
      // noMoreText: languageCT(context).mSmartNoMoreText,
      // infoText: languageCT(context).mSmartUpdateAtText + "%T",
      bgColor: Colors.transparent,
      infoColor: Color(0xFF999999),
      textColor: Color(0xFF666666),
      float: false,
      enableHapticFeedback: true,
    );
  }

  @override
  Footer dfFooter(BuildContext context) {
    return ClassicalFooter(
      enableInfiniteLoad: true,
      // loadText: languageCT(context).mSmartPushToLoadText,
      // loadReadyText: languageCT(context).mSmartReleaseToLoadText,
      // loadingText: languageCT(context).mSmartLoadingText,
      // loadedText: languageCT(context).mSmartLoadedText,
      // loadFailedText: languageCT(context).mSmartLoadFailedText,
      // noMoreText: languageCT(context).mLoadingMoreNoDataText,
      // infoText: languageCT(context).mSmartLoadingAtText,
      bgColor: Colors.transparent,
      infoColor: Color(0xFF999999),
      textColor: Color(0xFF666666),
      enableHapticFeedback: true,
    );
  }

  @override
  int dfPageListIndex(BuildContext context) {
    return 1;
  }

  @override
  int dfPageListSize(BuildContext context) {
    return 20;
  }
}
