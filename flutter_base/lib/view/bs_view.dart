import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';

/*

///数据逻辑
///by grus95
class TestVM extends BsViewModel{
 @override
  void buildCompleted(BuildContext context) {

  }

  @override
  void disposeVM() {

  }

  @override
  void initVM(BuildContext context) {

  }

}


///UI界面
///by grus95
class TestPage extends BsView<TestVM>{
  @override
  Widget buildScaffold(BuildContext context) {
    return viewModelCS(
      (ct, vm, cd) => Scaffold(
        appBar: _onPageAppBarView(ct, vm),
        body: _onPageBodyView(ct, vm),
      ),
    );
  }

  ///导航栏
  PreferredSizeWidget _onPageAppBarView(BuildContext context, TestVM vm) {
    return AutoAppBarView(
      titleName: '界面标题',
    );
  }

  ///内容
  Widget _onPageBodyView(BuildContext context, TestVM vm) {
    return Container();
  }

  @override
  TestVM buildViewModel(BuildContext context) {
    return TestVM();
  }

}

*/

///基类
abstract class BsView<T extends BsViewModel> extends StatelessWidget {
  BsView({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return _buildScaffold(context);
  }

  ///处理未使用viewModel的情况
  Widget _buildScaffold(BuildContext context) {
    List<SingleChildWidget> _providers = [];

    ///布局本身的数据模型
    _providers.add(vmProvider(context));

    ///布局其他的数据模型
    var _otherProviders = otherProviders(context);
    if (_otherProviders != null && _otherProviders.isNotEmpty) {
      _providers.addAll(_otherProviders);
    }

    ///判断布局类型
    bool unUseProviders = _providers == null || _providers.isEmpty;
    return unUseProviders
        ? buildScaffold(context)
        : MultiProvider(
            providers: _providers,
            builder: (context, child) {
              return buildScaffold(context);
            },
            child: null,
          );
  }

  ///布局本身的数据模型
  ///如果，重写该方法，则会影响原有的[VM];
  ///如果，需要增加其他[VM]，可重写[otherProviders]方法;
  @protected
  SingleChildWidget vmProvider(BuildContext context) => Provider<T>(
        create: (ct) {
          T viewModel = buildViewModel(context);
          viewModel?.init(ct);
          return viewModel;
        },
        lazy: lazyViewModel,
        dispose: (ct, vm) => vm?.disposeAll(),
      );

  ///布局其他的数据状态
  ///可自定义新增[VM]，默认为null
  ///[lazyViewModel]=false时，该数据才会实时创建
  @protected
  List<SingleChildWidget> otherProviders(BuildContext context) => null;

  ///默认在第一次读取值时调用，而不是第一次
  @protected
  bool get lazyViewModel => true;

  ///编译内容
  @protected
  Widget buildScaffold(BuildContext context);

  ///获取viewModel
  @protected
  T buildViewModel(BuildContext context);

////////////////////////////////////////////////////////////////////////////////
  ///附加功能

  ///获取当前vm
  T viewModelCT(BuildContext context, {bool listen}) => Provider.of<T>(
        context,
        listen: listen ?? false,
      );

  ///获取模式引用_Consumer
  Widget viewModelCS(
    ConsumerBuilder<T> builder, {
    Key key,
    Widget child,
  }) =>
      Consumer<T>(
        key: key,
        builder: builder,
        child: child,
      );

  ///获取模式引用_Selector
  Widget viewModelSL<S>(
    ValueWidgetBuilder<S> builder,
    S Function(BuildContext, T) selector, {
    Key key,
    ShouldRebuild<S> shouldRebuild,
    Widget child,
  }) =>
      Selector<T, S>(
        key: key,
        builder: builder,
        selector: selector,
        shouldRebuild: shouldRebuild,
        child: child,
      );
}

typedef Widget ConsumerBuilder<T>(BuildContext context, T value, Widget child);

///所有viewModel的父类，提供一些公共功能
abstract class BsViewModel {
  ///VM是否正在运行
  ///判断VM是否已经被回收
  bool _isRunning = false;

  bool get isRunning => _isRunning;

  @mustCallSuper
  void init(BuildContext context) {
    _isRunning = true;
    _buildPreparation(context);
  }

  ///布局编译前，仅调用一次
  void _buildPreparation(BuildContext context) {
    try {
      initVM(context);
      WidgetsBinding.instance.addPostFrameCallback((callback) {
        if (_isRunning) {
          _buildCompleted(context);
        }
      });
    } catch (e) {
      print('_buildPreparation异常-->>$e');
    }
  }

  ///布局编译后，仅调用一次
  void _buildCompleted(BuildContext context) {
    try {
      buildCompleted(context);
    } catch (e) {
      print('_buildCompleted异常-->>$e');
    }
  }

  ///初始化View前，仅调用一次
  @protected
  void initVM(BuildContext context);

  ///初始化View完成后，仅调用一次
  ///
  /// 如果需要监听每次编译布局完成后的事件，可以使用下面逻辑
  ///
  ///     WidgetUtil().asyncPrepare(context, false, (Rect rect) {
  ///       // widget渲染完成。
  ///       LogUtil.v('-widget渲染完成-commodityHeight->>${rect.height}');
  ///     });
  ///
  @protected
  void buildCompleted(BuildContext context);

  ///回收所有内容
  @mustCallSuper
  void disposeAll() {
    try {
      disposeVM();
    } catch (e) {
      print('disposeAll异常-->>$e');
    }
  }

  ///回收子类内容
  void disposeVM();
}
