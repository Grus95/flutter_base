import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_base/view/bs_view.dart';

///可输入的文本布局
///数据逻辑
///by grus95
class BsBottomEditTextViewVM extends BsViewModel {
  final TextEditingController controller = TextEditingController(text: '');

  BsBottomEditTextViewVM();

  @override
  void buildCompleted(BuildContext context) {}

  @override
  void disposeVM() {}

  @override
  void initVM(BuildContext context) {}

  ///更新键盘状态
  void onUpdateFocusNode(
    BuildContext context, {
    FocusNode focusNode,
    bool isShowInput,
  }) {
    // WidgetUtil().asyncPrepare(context, false, (Rect rect) {
    //   // widget渲染完成。
    //   if (focusNode != null && isShowInput == true) {
    //     FocusScope.of(context).requestFocus(focusNode);
    //   }
    // });
  }
}

///UI界面
///by grus95
class BsBottomEditTextView extends BsView<BsBottomEditTextViewVM> {
  final TextEditingController controller;
  final String titleName;
  final String placeholder;
  final String sureName;
  final int maxLines;
  final int maxLength;
  final TextInputType keyboardType;
  final TextInputAction textInputAction;
  final VoidCallback dismissCallBack;
  final ValueChanged<String> sureCallBack;
  final FocusNode focusNode;
  final ValueNotifier<bool> showInputVnr;
  final Widget child;
  final InputType type;

  BsBottomEditTextView({
    this.titleName,
    this.placeholder,
    this.sureName,
    this.maxLines,
    this.maxLength,
    this.keyboardType,
    this.textInputAction,
    this.dismissCallBack,
    this.sureCallBack,
    this.focusNode,
    @required this.controller,
    @required this.showInputVnr,
    @required this.child,
    this.type = InputType.text,
  });

  BsBottomEditTextView.text({
    this.titleName,
    this.placeholder,
    this.sureName,
    this.maxLines,
    this.maxLength,
    this.keyboardType,
    this.textInputAction,
    this.dismissCallBack,
    this.sureCallBack,
    this.focusNode,
    @required this.controller,
    @required this.showInputVnr,
    @required this.child,
  }) : this.type = InputType.text;

  BsBottomEditTextView.phone({
    this.titleName,
    this.placeholder,
    this.sureName,
    this.maxLines,
    this.maxLength,
    this.keyboardType,
    this.textInputAction,
    this.dismissCallBack,
    this.sureCallBack,
    this.focusNode,
    @required this.controller,
    @required this.showInputVnr,
    @required this.child,
  }) : this.type = InputType.phone;

  BsBottomEditTextView.number({
    this.titleName,
    this.placeholder,
    this.sureName,
    this.maxLines,
    this.maxLength,
    this.keyboardType,
    this.textInputAction,
    this.dismissCallBack,
    this.sureCallBack,
    this.focusNode,
    @required this.controller,
    @required this.showInputVnr,
    @required this.child,
  }) : this.type = InputType.number;

  BsBottomEditTextView.finance({
    this.titleName,
    this.placeholder,
    this.sureName,
    this.maxLines,
    this.maxLength,
    this.keyboardType,
    this.textInputAction,
    this.dismissCallBack,
    this.sureCallBack,
    this.focusNode,
    @required this.controller,
    @required this.showInputVnr,
    @required this.child,
  }) : this.type = InputType.finance;

  @override
  Widget buildScaffold(BuildContext context) {
    return viewModelCS(
      (ct, vm, cd) => _pageBodyView(ct, vm),
    );
  }

  ///内容
  Widget _pageBodyView(BuildContext context, BsBottomEditTextViewVM vm) {
    vm.onUpdateFocusNode(
      context,
      focusNode: focusNode,
      isShowInput: showInputVnr?.value ?? false,
    );
    return Stack(
      children: [
        child,
        ValueListenableBuilder<bool>(
          valueListenable: showInputVnr,
          builder: (context, showInput, child) {
            return Visibility(
              visible: showInput ?? false,
              child: child,
            );
          },
          child: GestureDetector(
            behavior: HitTestBehavior.translucent,
            onTap: () {
              FocusScope.of(context).unfocus();
              dismissCallBack?.call();
            },
            child: Container(
              color: Colors.black.withOpacity(0.2),
              alignment: Alignment.bottomCenter,
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.vertical(
                    top: Radius.circular(6),
                  ),
                ),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 15,
                        vertical: 12,
                      ),
                      child: Text(
                        titleName ?? '请输入',
                        style: TextStyle(
                          color: const Color(0xFF666666),
                          fontSize: 12,
                        ),
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                    Divider(height: 1, color: const Color(0xFFEBEBF0)),
                    Row(
                      children: [
                        Expanded(
                          child: CupertinoTextField(
                            controller: controller,
                            focusNode: focusNode,
                            placeholder: placeholder ?? '在此输入..',
                            placeholderStyle: TextStyle(
                              color: Color(0xFFCFCFCF),
                              fontSize: 12,
                            ),
                            style: TextStyle(
                              color: Color(0xFF202020),
                              fontSize: 14,
                            ),
                            padding: EdgeInsets.all(15),
                            decoration: BoxDecoration(
                              border: Border.symmetric(
                                horizontal: BorderSide(
                                  color: const Color(0xFFEBEBF0),
                                ),
                              ),
                            ),
                            maxLines: maxLines ?? 1,
                            maxLength: maxLength,
                            keyboardType: keyboardType,
                            textInputAction: textInputAction,
                            autofocus: true,
                            onSubmitted: (data) {
                              dismissCallBack?.call();
                            },
                          ),
                        ),
                        GestureDetector(
                          onTap: () {
                            String _data = controller?.text;
                            if (_data?.isNotEmpty == true) {
                            } else {
                              sureCallBack?.call(_data);
                              FocusScope.of(context).unfocus();
                            }
                          },
                          child: Container(
                            decoration: BoxDecoration(
                              color: Colors.green,
                              borderRadius: BorderRadius.circular(8),
                            ),
                            margin: EdgeInsets.symmetric(horizontal: 15),
                            padding: EdgeInsets.symmetric(
                              vertical: 6,
                              horizontal: 12,
                            ),
                            child: Text(
                              '确定',
                              style: TextStyle(
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }

  @override
  BsBottomEditTextViewVM buildViewModel(BuildContext context) {
    return BsBottomEditTextViewVM();
  }
}

///输入内容类型
enum InputType {
  ///金融
  finance,

  ///文本
  text,

  ///数字
  number,

  ///手机号码
  phone,
}
