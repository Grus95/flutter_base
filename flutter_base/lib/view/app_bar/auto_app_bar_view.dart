import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_base/view/app_bar/auto_app_bar_item_view.dart';

///导航栏
///by grus95
@deprecated
class AutoAppBarView extends StatelessWidget implements PreferredSizeWidget {
  final double appBarHeight;

  ///最左边布局
  final Widget leadingItem;

  ///最左边布局-点击
  final GestureTapCallback leadingOnTap;

  ///最左边布局-长按
  final GestureLongPressCallback leadingOnLongPress;

  ///最左边布局-是否显示
  final bool isShowLeading;

  ///返回按钮颜色
  final Color backColor;

  final List<AutoAppBarItemView> addLeadingView;

  final List<AutoAppBarItemView> addTrailingView;

  ///中间布局
  final Widget title;
  final String titleName;
  final Color titleColor;

  final Color backgroundColor;

  final Brightness brightness;

  final PreferredSizeWidget bottom;

  final double titleMaxWidth;

  final List<Widget> actions;

  AutoAppBarView({
    this.appBarHeight,
    this.leadingItem,
    this.leadingOnTap,
    this.leadingOnLongPress,
    this.backColor,
    this.isShowLeading,
    this.addLeadingView,
    this.addTrailingView,
    this.title,
    this.titleName,
    this.titleColor,
    this.backgroundColor,
    this.brightness = Brightness.dark,
    this.bottom,
    this.titleMaxWidth,
    this.actions,
  });

  @override
  Size get preferredSize => Size.fromHeight(
      (appBarHeight ?? 44) + (bottom?.preferredSize?.height ?? 0.0));

  @override
  Widget build(BuildContext context) {
    ///左边返回按钮
    Widget leading = AutoAppBarLeadingView(
      leadingItem: leadingItem,
      backColor: backColor,
      brightness: brightness ?? Brightness.dark,
      leadingOnTap: leadingOnTap,
      isShowLeading: isShowLeading,
    );

    ///左半边界面布局
    List<Widget> _addLeadingView = List();
    _addLeadingView.add(leading);
    if (addLeadingView != null && addLeadingView.isNotEmpty) {
      _addLeadingView.addAll(addLeadingView);
    }

    ///右半边界面布局
    List<Widget> _addTrailingView = List();
    if (addTrailingView != null && addTrailingView.isNotEmpty) {
      int size = addTrailingView.length - 1;
      for (int i = size; i >= 0; i--) {
        _addTrailingView.add(addTrailingView[i]);
      }
    }

    ///中间布局
    Widget _titleView = title ??
        AutoAppBarTitleView(
          titleName: titleName,
          color: titleColor,
        );

    return AppBar(
      leading: leading,
      title: _titleView,
      actions: actions ?? _addTrailingView,
      backgroundColor: backgroundColor ?? const Color(0xFFFF9710),
      brightness: brightness ?? Brightness.dark,
      elevation: 0,
    );
  }
}

///导航栏中间布局
class AutoAppBarLeadingView extends StatelessWidget {
  ///最左边布局
  final Widget leadingItem;

  ///返回按钮颜色
  final Color backColor;

  ///最左边布局-点击
  final GestureTapCallback leadingOnTap;

  ///最左边布局-是否显示
  final bool isShowLeading;

  ///背景类型
  final Brightness brightness;

  const AutoAppBarLeadingView(
      {Key key,
      this.leadingItem,
      this.backColor,
      this.leadingOnTap,
      this.brightness,
      this.isShowLeading})
      : super(key: key);

  Widget get backLightView => Image.asset(
        'static/webp/ic_back_light.webp',
      );

  Widget get backDarkView => Image.asset(
        'static/webp/ic_back_dark.webp',
      );

  // Widget get backSvgView => SvgPicture.asset(
  //       'static/svg/ic_back.svg',
  //       width: 22,
  //       height: 22,
  //       color: backSvgColor,
  //     );
  //
  // Color get backSvgColor =>
  //     backColor ??
  //     (brightness == Brightness.dark
  //         ? const Color(0xFFFFFEFE)
  //         : const Color(0xFF202020));

  @override
  Widget build(BuildContext context) {
    bool _canPop = Navigator.of(context).canPop();
    Widget _dfBack = leadingItem ??
        (brightness == Brightness.dark ? backLightView : backDarkView);
    Widget leading = AutoAppBarItemView(
      item: _dfBack,
      itemOnTap: leadingOnTap ??
          () {
            if (_canPop) {
              Navigator.of(context).pop();
            }
          },
      isShowItem: isShowLeading ?? _canPop,
    );
    return leading;
  }
}

///导航栏中间布局
class AutoAppBarTitleView extends StatelessWidget {
  final String titleName;
  final double fontSizeAdapter;
  final Color color;
  final FontWeight fontWeight;
  final TextAlign textAlign;
  final TextStyle style;

  const AutoAppBarTitleView({
    Key key,
    this.titleName,
    this.fontSizeAdapter,
    this.color,
    this.fontWeight,
    this.textAlign,
    this.style,
  }) : super(key: key);

  double get fontSize => fontSizeAdapter ?? 16;

  @override
  Widget build(BuildContext context) {
    return Text(
      titleName ?? '',
      textAlign: textAlign ?? TextAlign.center,
      style: style ??
          TextStyle(
            fontSize: fontSize,
            color: color ?? Color(0xFFFFFEFE),
            fontWeight: fontWeight ?? FontWeight.normal,
          ),
    );
  }
}
