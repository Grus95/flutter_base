import 'package:flutter/material.dart';

///导航栏上的每个item
///by grus95
@deprecated
class AutoAppBarItemView extends StatelessWidget {
  ///布局
  final Widget item;

  ///布局-点击
  final GestureTapCallback itemOnTap;

  ///布局-长按
  final GestureLongPressCallback itemOnLongPress;

  ///布局-是否显示
  final bool isShowItem;

  final double width;

  AutoAppBarItemView({
    this.item,
    this.itemOnTap,
    this.itemOnLongPress,
    this.isShowItem,
    this.width,
  });

  double get sizeAppBarItemHeight => 44.0;

  double get sizeAppBarItemWidth => width ?? sizeAppBarItemHeight + 8;

  Widget buildScaffold(BuildContext context) {
    return isShowItem ?? true
        ? GestureDetector(
            onTap: itemOnTap,
            onLongPress: itemOnLongPress,
            child: Container(
              color: Colors.transparent,
              width: sizeAppBarItemWidth,
              height: sizeAppBarItemHeight,
              alignment: Alignment.center,
              child: item ?? Container(),
            ),
          )
        : Container();
  }

  @override
  Widget build(BuildContext context) {
    return buildScaffold(context);
  }
}
