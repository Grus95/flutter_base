import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

///数字输入的精确控制
class NumberLimitFormatter extends TextInputFormatter {
  ///最小数
  final int minNum;

  ///最大数
  final int maxNum;

  NumberLimitFormatter({
    this.minNum,
    this.maxNum,
  });

  RegExp exp = RegExp("[0-9]");
  static const String ZERO = "0";

  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    ///输入完全删除
    if (newValue.text.isEmpty) {
      return TextEditingValue();
    }

    ///只允许输入数字
    if (!exp.hasMatch(newValue.text)) {
      return oldValue;
    }

    ///如果第1位为0，并且长度大于1，排除00,01-09所有非法输入
    if ((newValue.text.startsWith(ZERO) && newValue.text.length >= 1)) {
      return oldValue;
    }

    ///如果值小于最小值
    if (minNum != null && newValue.text.length > 0) {
      int num = int.tryParse(newValue.text);
      if (num != null && minNum > num) {
        return oldValue;
      }
    }

    ///如果值大于最大值
    if (maxNum != null && newValue.text.length > 0) {
      int num = int.tryParse(newValue.text);
      if (num != null && maxNum < num) {
        return oldValue;
      }
    }

    return newValue;
  }
}

///输入的长度超出tip控制
///
///  inputFormatters: [
///                 LengthWithTipInputFormatter(
///                   maxLength: 20,
///                   overstepCallback: () {
///                     showToast('字数过多');
///                   },
///                 ),
///               ],
class LengthWithTipInputFormatter extends TextInputFormatter {
  ///最大长度
  final int maxLength;
  final VoidCallback overstepCallback;

  LengthWithTipInputFormatter({
    this.maxLength,
    this.overstepCallback,
  });

  RegExp exp = RegExp("[0-9]");
  static const String ZERO = "0";

  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    ///输入完全删除
    if (newValue.text.isEmpty) {
      return TextEditingValue();
    }

    // Return the new value when the old value has not reached the max
    // limit or the old value is composing too.
    if (newValue.composing.isValid) {
      if (maxLength != null &&
          maxLength > 0 &&
          oldValue.text.characters.length == maxLength &&
          !oldValue.composing.isValid) {
        overstepCallback?.call();
        return oldValue;
      }
      return newValue;
    }
    if (maxLength != null &&
        maxLength > 0 &&
        newValue.text.characters.length > maxLength) {
      // If already at the maximum and tried to enter even more, keep the old
      // value.
      if (oldValue.text.characters.length == maxLength) {
        overstepCallback?.call();
        return oldValue;
      }
      overstepCallback?.call();
      return oldValue;
    }
    return newValue;
  }
}
