///
//  Generated code. Do not modify.
//  source: test/test.proto
//
// @dart = 2.3
// ignore_for_file: camel_case_types,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type

import 'dart:core' as $core;

import 'package:fixnum/fixnum.dart' as $fixnum;
import 'package:protobuf/protobuf.dart' as $pb;

import 'test.pbenum.dart';

export 'test.pbenum.dart';

class PbUser extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('PbUser', package: const $pb.PackageName('com.base.user'), createEmptyInstance: create)
    ..a<$fixnum.Int64>(1, 'uid', $pb.PbFieldType.OU6, defaultOrMaker: $fixnum.Int64.ZERO)
    ..aOS(2, 'nick')
    ..e<PbGender>(3, 'gender', $pb.PbFieldType.OE, defaultOrMaker: PbGender.UN_KNOWN, valueOf: PbGender.valueOf, enumValues: PbGender.values)
    ..a<$core.int>(4, 'level', $pb.PbFieldType.O3)
    ..hasRequiredFields = false
  ;

  PbUser._() : super();
  factory PbUser() => create();
  factory PbUser.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory PbUser.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  PbUser clone() => PbUser()..mergeFromMessage(this);
  PbUser copyWith(void Function(PbUser) updates) => super.copyWith((message) => updates(message as PbUser));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static PbUser create() => PbUser._();
  PbUser createEmptyInstance() => create();
  static $pb.PbList<PbUser> createRepeated() => $pb.PbList<PbUser>();
  @$core.pragma('dart2js:noInline')
  static PbUser getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<PbUser>(create);
  static PbUser _defaultInstance;

  @$pb.TagNumber(1)
  $fixnum.Int64 get uid => $_getI64(0);
  @$pb.TagNumber(1)
  set uid($fixnum.Int64 v) { $_setInt64(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasUid() => $_has(0);
  @$pb.TagNumber(1)
  void clearUid() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get nick => $_getSZ(1);
  @$pb.TagNumber(2)
  set nick($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasNick() => $_has(1);
  @$pb.TagNumber(2)
  void clearNick() => clearField(2);

  @$pb.TagNumber(3)
  PbGender get gender => $_getN(2);
  @$pb.TagNumber(3)
  set gender(PbGender v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasGender() => $_has(2);
  @$pb.TagNumber(3)
  void clearGender() => clearField(3);

  @$pb.TagNumber(4)
  $core.int get level => $_getIZ(3);
  @$pb.TagNumber(4)
  set level($core.int v) { $_setSignedInt32(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasLevel() => $_has(3);
  @$pb.TagNumber(4)
  void clearLevel() => clearField(4);
}

