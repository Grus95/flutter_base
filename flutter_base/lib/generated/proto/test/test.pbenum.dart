///
//  Generated code. Do not modify.
//  source: test/test.proto
//
// @dart = 2.3
// ignore_for_file: camel_case_types,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type

// ignore_for_file: UNDEFINED_SHOWN_NAME,UNUSED_SHOWN_NAME
import 'dart:core' as $core;
import 'package:protobuf/protobuf.dart' as $pb;

class PbLevel extends $pb.ProtobufEnum {
  static const PbLevel NORMAL = PbLevel._(0, 'NORMAL');
  static const PbLevel VIP_1 = PbLevel._(1, 'VIP_1');

  static const $core.List<PbLevel> values = <PbLevel> [
    NORMAL,
    VIP_1,
  ];

  static final $core.Map<$core.int, PbLevel> _byValue = $pb.ProtobufEnum.initByValue(values);
  static PbLevel valueOf($core.int value) => _byValue[value];

  const PbLevel._($core.int v, $core.String n) : super(v, n);
}

class PbGender extends $pb.ProtobufEnum {
  static const PbGender UN_KNOWN = PbGender._(0, 'UN_KNOWN');
  static const PbGender MALE = PbGender._(1, 'MALE');
  static const PbGender FEMALE = PbGender._(2, 'FEMALE');

  static const $core.List<PbGender> values = <PbGender> [
    UN_KNOWN,
    MALE,
    FEMALE,
  ];

  static final $core.Map<$core.int, PbGender> _byValue = $pb.ProtobufEnum.initByValue(values);
  static PbGender valueOf($core.int value) => _byValue[value];

  const PbGender._($core.int v, $core.String n) : super(v, n);
}

