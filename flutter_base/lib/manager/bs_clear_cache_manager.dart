import 'dart:io';

import 'package:path_provider/path_provider.dart';

///清理缓存
///by grus95
class BsClearCacheManager {
  ///加载缓存
  static Future<String> loadCache() async {
    String _cacheSizeStr;
    try {
      Directory tempDir = await getTemporaryDirectory();
      double value = await _getTotalSizeOfFilesInDir(tempDir);
      /*tempDir.list(followLinks: false,recursive: true).listen((file){
          //打印每个缓存文件的路径
        print(file.path);
      });*/
      print('临时目录大小: ' + value.toString());
      _cacheSizeStr = _renderSize(value);
    } catch (err) {
      print(err);
    }
    return _cacheSizeStr;
  }

  ///格式化文件大小
  static String _renderSize(double value) {
    if (null == value) {
      return '0';
    }
    List<String> unitArr = List()..add('B')..add('K')..add('M')..add('G');
    int index = 0;
    while (value > 1024) {
      index++;
      value = value / 1024;
    }
    String size = value.toStringAsFixed(2);
    return size + unitArr[index];
  }

  /// 递归方式 计算文件的大小
  static Future<double> _getTotalSizeOfFilesInDir(
      final FileSystemEntity file) async {
    try {
      if (file is File) {
        int length = await file.length();
        return double.parse(length.toString());
      }
      if (file is Directory) {
        final List<FileSystemEntity> children = file.listSync();
        double total = 0;
        if (children != null)
          for (final FileSystemEntity child in children)
            total += await _getTotalSizeOfFilesInDir(child);
        return total;
      }
      return 0;
    } catch (e) {
      print(e);
      return 0;
    }
  }

  ///清理缓存
  static Future<void> clearCache({
    void Function() suc,
    void Function() fai,
    void Function() startClear,
    void Function() endClear,
  }) async {
    //此处展示加载loading
    startClear?.call();
    try {
      Directory tempDir = await getTemporaryDirectory();
      //删除缓存目录
      await _delDir(tempDir);
      suc?.call();
    } catch (e) {
      print(e);
      fai?.call();
    } finally {
      //此处隐藏加载loading
      endClear?.call();
    }
  }

  ///递归方式删除目录
  static Future<void> _delDir(FileSystemEntity file) async {
    try {
      if (file is Directory) {
        final List<FileSystemEntity> children = file.listSync();
        for (final FileSystemEntity child in children) {
          await _delDir(child);
        }
      }
      await file.delete();
    } catch (e) {
      print(e);
    }
  }
}
