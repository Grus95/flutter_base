///Api接口字段Mixin
///新建必须以 [i]开头，
///
///by gurs95
class ApiInterface {
  ///服务host
  static const String host = 'https://baidu.com';

  ///版本号
  static const String version = 'v0';

  ///端口号
  static const String port = '59999';

  //////////////////////////////////////////////////////////////////////////////
  //api接口

  ///用户登录-POST
  ///退出登录-DELETE
  static const iUserAdminSession = '/user_admin/session/';

}
