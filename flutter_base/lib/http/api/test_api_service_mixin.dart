
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_base/http/bs_http_mixin.dart';

import 'test_api_interface.dart';
import '../test_http_manager.dart';

///API接口服务Mixin
///新建必须以 [api]开头，
///
///by gurs95
class ApiServiceMixin {
  final HttpManager _$http = HttpManager.$http;

  ///获取当前后端的地址
  String get bsApiHost => _$http.bsApiHost;

  ///默认每页数量
  String get _dfCount => '20';

  /////////////////////////////接口服务开始//////////////////

  ///用户登录状态相关-用户登录
  Future<HttpApiBean> apiUserSessionPost(
    BuildContext context, {
    CancelToken cancelToken,
    void suc(String tBean),
    void fai(HttpApiBean resBean),
  }) {
    return _$http.requestBean<String>(
      context,
      ApiInterface.iUserAdminSession,
      method: BsHttpMethod.post,
      conversionEachBean: (data) => null,
      cancelToken: cancelToken,
      suc: suc,
      fai: fai,
    );
  }
}
