import 'dart:convert';
import 'dart:io';

import 'package:device_info/device_info.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_base/flutter_base.dart';
import 'package:flutter_custom_dialog/flutter_custom_dialog.dart';
import 'package:package_info/package_info.dart';

///请求数据成功后，转换成必要数据bean
typedef OnResponse2BeanSucCall<T> = T Function(
  BuildContext context,
  Response response,
);

///请求数据失败后，转换成必要数据bean
typedef OnResponse2BeanFailCall<T> = T Function(
  BuildContext context,
  Response response,
);

///请求异常回调
typedef OnErrorTipCall = YYDialog Function(BuildContext context, String msg);

///请求加载回调
typedef OnLoadingTipCall = YYDialog Function(BuildContext context);

///请求开始
typedef OnRequestStartCall = Function(BuildContext context);

///结束
typedef OnRequestEndCall = Function(BuildContext context);

///网络请求
///使用参考[TestHttp]
///by grus95
mixin BsHttpMixin {
  ///用户TOKEN
  final String authorization = 'Authorization';

  ///时间戳
  final String xTimestamp = 'X-TIMESTAMP';

  ///系统以及版本号，比如 iOS 14.1
  final String userAgent = 'User-Agent';

  ///content_type
  final String contentType = 'Content-Type';

  ///Accept
  final String accept = 'Accept';

  ///protobuf 类型
  final String protobufContentType = _protobufContentType;

  ///protobuf 类型
  static const _protobufContentType = 'application/x-protobuf;charset=UTF-8';

  ///请求方式-map
  final Map<BsHttpMethod, String> bsHttpMethodMap = {
    BsHttpMethod.post: 'post',
    BsHttpMethod.get: 'get',
    BsHttpMethod.put: 'put',
    BsHttpMethod.delete: 'delete',
    BsHttpMethod.patch: 'patch',
  };

  ///请求体内容类型-map
  final Map<BsHttpContentType, String> bsHttpContentTypeMap = {
    BsHttpContentType.json: Headers.jsonContentType,
    BsHttpContentType.form: Headers.formUrlEncodedContentType,
    BsHttpContentType.string: Headers.formUrlEncodedContentType,
    BsHttpContentType.protobuf: _protobufContentType,
  };

  ///当前设备信息
  String _deviceInfo;

  ///host服务地址
  String readyBsHost();

  ///端口号，如果有
  String readyBsPort();

  ///api版本，如果有
  String readyBsVersion();

  ///是否开启debug
  bool readyDebugEnable();

  ///请求api接口的host
  ///用于自定义拼凑地址[host:port/version]后，再加api接口字段,
  ///填充到[BsHttpMixin.sendRequest]方法的[path]字段
  Future<String> readyBsApiHost(
    BuildContext context, {
    String bsHost,
    String bsPort,
    String bsVersion,
    bool debug,
  });

  String _bsApiHost;

  ///对外api地址
  String get bsApiHost => _bsApiHost;

  ///默认网络请求对象
  Future<Dio> readyDio(BuildContext context);

  ///发起网络请求对象
  static Dio _dior;

  ///对外dio
  Dio get dior => _dior;

  ///加载提示弹窗
  YYDialog _mLoadingDialog;

  ///接口访问加载弹窗
  YYDialog iLoadingDialog(BuildContext context) => YYDialog().build(context)
    ..borderRadius = 6.0
    ..widget(Padding(
      padding: const EdgeInsets.all(40),
      child: CupertinoActivityIndicator(),
    ));

  ///错误提示弹窗
  YYDialog _mErrorTipDialog;

  ///错误提示弹窗
  YYDialog iErrorTipDialog(BuildContext context, String msg) =>
      YYDialog().build(context)
        ..width = 220
        ..borderRadius = 6.0
        ..dismissCallBack = () {
          _mErrorTipDialog = null;
        }
        ..text(
          padding: const EdgeInsets.all(20),
          alignment: Alignment.center,
          text: msg ?? '',
          color: Colors.red,
          fontSize: 14.0,
          fontWeight: FontWeight.w500,
        );

  ///初始化-相关信息
  @mustCallSuper
  Future<void> init(BuildContext context) async {
    _bsApiHost = await readyBsApiHost(
      context,
      bsHost: readyBsHost(),
      bsPort: readyBsPort(),
      bsVersion: readyBsVersion(),
      debug: readyDebugEnable(),
    );
    _dior = await readyDio(context) ?? Dio();
    _deviceInfo = await getDeviceInfo();
    print('--BsHttpMixin--初始化');
  }

  ///发起网络请求
  ///[ path] 请求地址
  ///[ method] 请求方法
  ///[ contentType] 请求内容体类型
  ///[ paramsData] 请求内容体数据
  ///[ header] 请求头
  ///[ option] 配置
  ///[ showTip] 是否显示tip
  ///[ cancelToken] 关闭请求token
  Future<T> sendRequest<T>(
    BuildContext context,
    String path, {
    @required BsHttpMethod method,
    @required OnResponse2BeanSucCall<T> sucResponseCall,
    @required OnResponse2BeanFailCall<T> failResponseCall,
    dynamic paramsData,
    Map<String, dynamic> queryParam,
    Map<String, dynamic> header,
    Options option,
    CancelToken cancelToken,
    OnErrorTipCall onErrorTipCall,
    OnLoadingTipCall onLoadingTipCall,
    bool showLoadingEnable,
  }) async {
    assert(context != null);

    ///开始请求
    if (showLoadingEnable == true && _mLoadingDialog == null) {
      _mLoadingDialog =
          (onLoadingTipCall.call(context) ?? iLoadingDialog(context))..show();
    }

    ///配置
    Options _option = option ?? Options();
    _option.method = bsHttpMethodMap[method ?? BsHttpMethod.get];
    if (header != null && header.isNotEmpty) {
      _option.headers.addAll(header);
    }

    ///设置请求参数
    var _data = paramsData;

    ///设置请求体
    Response _response;

    ///数据
    T rBean;

    String _errorMsg;

    ///开始请求接口
    try {
      _response = await _dior.request(
        path,
        data: _data,
        queryParameters: queryParam,
        options: _option,
        cancelToken: cancelToken,
      );
      rBean = sucResponseCall?.call(context, _response);
    } on DioError catch (e) {
      if (e.response != null) {
        _response = e.response;
      } else {
        _response = Response(statusCode: 666);
        _errorMsg = 'Server is restarting(服务器正在重启)';
      }
      rBean = failResponseCall?.call(context, _response);

      ///判断错误类型
      switch (e.type) {
        case DioErrorType.CONNECT_TIMEOUT:
          _errorMsg = 'Link Timeout(打开连接超时)';
          break;
        case DioErrorType.SEND_TIMEOUT:
          _errorMsg = 'Link Timeout(发送连接超时)';
          break;
        case DioErrorType.RECEIVE_TIMEOUT:
          _errorMsg = 'Link Timeout(接收连接超时)';
          break;
        case DioErrorType.RESPONSE:
        case DioErrorType.CANCEL:
        case DioErrorType.DEFAULT:
          if (e is SocketException) {
            _errorMsg = (e as SocketException).osError.errorCode.toString();
          } else {
            _errorMsg = e?.message;
          }
          break;
      }

      String _msgLog =
          '🥱网络⚛️请求错误❌-->${e?.toString()}-->$_errorMsg-->$_response';
      if (DioErrorType.CANCEL == e.type) {
        _msgLog = 'app系统内部主动关闭了网络请求！${e?.toString()}';
      }
      print(_msgLog);
    } finally {
      ///结束请求
      if (showLoadingEnable == true) {
        _mLoadingDialog?.dismiss();
        _mLoadingDialog = null;
      }

      ///显示异常
      if (_errorMsg != null &&
          _errorMsg.isNotEmpty &&
          _mErrorTipDialog?.isShowing != true) {
        _mErrorTipDialog = (onErrorTipCall?.call(context, _errorMsg) ??
            iErrorTipDialog(context, _errorMsg))
          ..show();
      }
    }
    return rBean;
  }

  /// 获取设备信息
  Future<String> getDeviceInfo() async {
    if (_deviceInfo == null) {
      Map<String, dynamic> _devInfo = Map();
      var packageInfo = await PackageInfo.fromPlatform();
      DeviceInfoPlugin deviceInfoPlugin = DeviceInfoPlugin();
      if (Platform.isAndroid) {
        AndroidDeviceInfo androidDeviceInfo =
            await deviceInfoPlugin.androidInfo;
        print('---------------获取到安卓设备信息---------------');
        print('设备ID：${androidDeviceInfo.androidId}');
        print('设备名称：${androidDeviceInfo.model}');
        print('系统类型：${androidDeviceInfo.brand}');
        print('系统版本：${androidDeviceInfo.fingerprint}');
        print('app版本：${packageInfo.version}+${packageInfo.buildNumber}');
        _devInfo['info'] = {
          'devId': '${androidDeviceInfo.androidId}',
          'devName': '${androidDeviceInfo.model}',
          'sysType': '${androidDeviceInfo.brand}',
          'sysVersion': '${androidDeviceInfo.fingerprint}',
          'appVersion': '${packageInfo.version}+${packageInfo.buildNumber}',
        };
      } else if (Platform.isIOS) {
        IosDeviceInfo iosDeviceInfo = await deviceInfoPlugin.iosInfo;
        print('---------------获取到iOS设备信息---------------');
        print('设备ID：${iosDeviceInfo.identifierForVendor}');
        print('设备名称：${iosDeviceInfo.name}(${iosDeviceInfo.utsname.machine})');
        print('系统类型：${iosDeviceInfo.systemName}');
        print('系统版本：${iosDeviceInfo.systemVersion}');
        print('app版本：${packageInfo.version}+${packageInfo.buildNumber}');
        _devInfo['info'] = {
          'devId': '${iosDeviceInfo.identifierForVendor}',
          'devName': '${iosDeviceInfo.name}(${iosDeviceInfo.utsname.machine})',
          'sysType': '${iosDeviceInfo.systemName}',
          'sysVersion': '${iosDeviceInfo.systemVersion}',
          'appVersion': '${packageInfo.version}+${packageInfo.buildNumber}',
        };
      }
      print('_devInfo:$_devInfo');

      _deviceInfo = Uri.encodeFull(json.encode(_devInfo));
    }
    return _deviceInfo;
  }
}

///请求方式
enum BsHttpMethod {
  post,
  get,
  put,
  delete,
  patch,
}

///请求体内容类型
enum BsHttpContentType {
  json,
  form,
  string,
  protobuf,
}
