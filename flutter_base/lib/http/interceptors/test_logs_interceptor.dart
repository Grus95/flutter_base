import 'package:dio/dio.dart';
import 'package:flutter_base/bean/AppDebugApiBean.dart';
import 'package:sp_util/sp_util.dart';

import '../test_http_manager.dart';

const String DEVELOPERS_DEBUG_DATA_KEY = 'developers_debug_data_key';

///Log 拦截器
class LogsInterceptor extends InterceptorsWrapper {
  ///是否打印日志
  final bool printLogsEnable;

  ///是否记录日志
  final bool recordLogsEnable;

  ///记录日志的最大条数
  final int recordLogsMaxNum;

  LogsInterceptor({
    this.printLogsEnable = false,
    this.recordLogsEnable = false,
    this.recordLogsMaxNum = 30,
  });

  String interfacePath(String path) {
    var interface = path?.replaceAll(HttpManager.$http.bsApiHost, '');
    return '$interface';
  }

  @override
  onRequest(RequestOptions options) async {
    return options;
  }

  @override
  onResponse(Response response) async {
    await _printDataLog(response?.request, response);
    return response;
  }

  @override
  onError(DioError err) async {
    String tag = '拦截器|Error';
    print(tag + '请求异常: ' + err.toString());
    print(tag + '请求异常信息: 具体分析如下面内容');
    await _printDataLog(err?.request, err?.response);
    return err;
  }

  ///打印请求数据日志
  Future<void> _printDataLog(RequestOptions request, Response response) async {
    RequestOptions _request = request ?? response?.request;
    String interface = interfacePath(response?.request?.path);
    String parameter = '${response?.request?.data}';

    ///打印日志
    if (printLogsEnable) {
      String tag0 = '拦截器|请求数据';
      print(tag0 + '拦截器--开始--');
      print(tag0 + 'path-->${_request?.path}');
      print(tag0 + 'headers-->${_request?.headers}');
      print(tag0 + 'method-->${_request?.method}');
      print(tag0 + 'queryParameters-->${_request?.queryParameters}');
      print(tag0 + 'data-->${_request?.data}');

      String tag = '拦截器|接收数据';
      String apiResults = '⚠️服务器⚠️没响应数据，后端🧍‍♂️接口问题，我们❌不背锅！';
      if (response?.data != null) {
        apiResults = '${response?.data}';
      }

      print(tag + 'path-->$interface');
      print(tag + 'parameter-->$parameter');
      print(tag + 'response-->$apiResults');
      print(tag + '拦截器--结束--');
    }

    ///记录日志到本地
    if (recordLogsEnable) {
      AppDebugApiBean _debugApiBean = AppDebugApiBean(
        interface: interface,
        parameter: parameter,
        time: DateTime.now().toString(),
        apiData: response?.toString(),
      );
      await _cacheDebugDataAction(_debugApiBean);
    }
  }

  ///缓存数据
  Future<void> _cacheDebugDataAction(AppDebugApiBean debugApiBean) async {
    AppDebugApiBean apiBean = SpUtil.getObj<AppDebugApiBean>(
      DEVELOPERS_DEBUG_DATA_KEY,
      (json) => AppDebugApiBean.fromJson(json),
      defValue: AppDebugApiBean(children: List()),
    );

    ///缓存数据条数[keepNumber]
    int keepNumber = recordLogsMaxNum ?? 30;
    if (apiBean?.children != null) {
      if (apiBean.children.length == 0) {
        apiBean?.children?.add(debugApiBean);
      } else {
        apiBean?.children?.insert(0, debugApiBean);
      }
      if (apiBean.children.length > keepNumber) {
        apiBean?.children?.removeRange(
            keepNumber, apiBean?.children?.length ?? (keepNumber + 1) - 1);
      }
      await SpUtil.putObject(DEVELOPERS_DEBUG_DATA_KEY, apiBean);
    }
  }
}
