import 'package:flutter/material.dart';
import 'package:flutter_base/flutter_base.dart';
import 'package:flutter_base/http/bs_http_mixin.dart';

import 'api/test_api_interface.dart';
import 'interceptors/test_logs_interceptor.dart';

typedef ConversionEachBean<T> = T Function(dynamic data);

/// 网络请求管理类
///
/// 初始化使用:
/// ``````````````
/// HttpManager.$http.init(context);
/// ``````````````
///
/// by grus95
class HttpManager with BsHttpMixin {
  static HttpManager _instance;

  HttpManager._internal();

  static HttpManager _getInstance() {
    if (_instance == null) {
      _instance = HttpManager._internal();
    }
    return _instance;
  }

  factory HttpManager() => _getInstance();

  static HttpManager get $http => _getInstance();

  @override
  String readyBsHost() {
    return ApiInterface.host;
  }

  @override
  String readyBsVersion() {
    return ApiInterface.version;
  }

  @override
  String readyBsPort() {
    return ApiInterface.port;
  }

  @override
  bool readyDebugEnable() {
    return true;
  }

  @override
  Future<String> readyBsApiHost(
    BuildContext context, {
    String bsHost,
    String bsPort,
    String bsVersion,
    bool debug,
  }) {
    String _apiHost = '';
    if (bsHost?.isNotEmpty == true) {
      _apiHost += bsHost;
    }
    if (bsPort?.isNotEmpty == true) {
      _apiHost += ':' + bsPort;
    }
    if (bsVersion?.isNotEmpty == true) {
      _apiHost += '/' + bsVersion;
    }
    return Future.value(_apiHost);
  }

  @override
  Future<Dio> readyDio(BuildContext context) {
    var _dio = Dio();
    _dio.options = BaseOptions(
      connectTimeout: 25 * 1000,
      responseType: ResponseType.json,
    );
    _dio.interceptors.add(LogsInterceptor(
      printLogsEnable: true,
      recordLogsEnable: false,
    ));
    return Future.value(_dio);
  }

  @override
  Future<void> init(BuildContext context) {
    return super.init(context);
  }

  ///发起网络请求
  ///[ interface] 请求接口
  ///[ method] 请求方法
  ///[ contentType] 请求内容体类型
  ///[ paramsData] 请求内容体数据
  ///[ header] 请求头
  ///[ option] 配置
  ///[ showTip] 是否显示tip
  ///[ cancelToken] 关闭请求token
  Future<HttpApiBean<dynamic>> _request(
    BuildContext context,
    String interface, {
    @required BsHttpMethod method,
    OnResponse2BeanSucCall<HttpApiBean> sucResponseCall,
    OnResponse2BeanFailCall<HttpApiBean> failResponseCall,
    dynamic paramsData,
    Map<String, dynamic> queryParam,
    Map<String, dynamic> header,
    Options option,
    CancelToken cancelToken,
    OnErrorTipCall onErrorTipCall,
    OnLoadingTipCall onLoadingTipCall,
    bool showLoadingEnable,
  }) async {
    ///设置请求头
    String _devInfo = await getDeviceInfo();
    Map<String, dynamic> _header = {
      xTimestamp: DateTime.now().millisecondsSinceEpoch,
      userAgent: _devInfo,
      authTokenKey: _authToken,
    };
    if (header != null && header.isNotEmpty) {
      _header.addAll(_header);
    }

    ///获取返回结果数据
    HttpApiBean _apiBean = await sendRequest<HttpApiBean>(
      context,
      bsApiHost + interface,
      method: method,
      sucResponseCall: sucResponseCall ??
          (BuildContext context, Response<dynamic> response) {
            var _rData = response?.data;
            var _bean = HttpApiBean();
            if (_rData != null && _rData is Map) {
              var _rDataMap = _rData;
              _bean
                ..code = _rDataMap[HttpApiBean.code_]
                ..success = _rDataMap[HttpApiBean.success_]
                ..type = _rDataMap[HttpApiBean.type_]
                ..errorMsg = _rDataMap[HttpApiBean.errorMsg_]
                ..detailError = _rDataMap[HttpApiBean.detailError_]
                ..data = _rDataMap[HttpApiBean.data_];
            }

            return _bean;
          },
      failResponseCall: failResponseCall ??
          (BuildContext context, Response<dynamic> response) {
            var _bean = HttpApiBean(
              code: response.statusCode,
              success: false,
              type: null,
              errorMsg: '${response.statusMessage}',
              detailError: '${response.toString()}',
              data: null,
            );
            return _bean;
          },
      paramsData: paramsData,
      queryParam: queryParam,
      header: _header,
      option: option,
      cancelToken: cancelToken,
      onErrorTipCall: onErrorTipCall,
      onLoadingTipCall: onLoadingTipCall,
      showLoadingEnable: showLoadingEnable,
    );
    return _apiBean;
  }

  ///发起网络请求
  ///[ interface] 请求接口
  ///[ method] 请求方法
  ///[ contentType] 请求内容体类型
  ///[ paramsData] 请求内容体数据
  ///[ header] 请求头
  ///[ option] 配置
  ///[ showTip] 是否显示tip
  ///[ cancelToken] 关闭请求token
  Future<HttpApiBean<T>> requestBean<T>(
    BuildContext context,
    String interface, {
    @required BsHttpMethod method,
    @required ConversionEachBean<T> conversionEachBean,
    Map cvApiBean2Data(HttpApiBean apiBean),
    OnResponse2BeanSucCall<HttpApiBean> sucResponseCall,
    OnResponse2BeanFailCall<HttpApiBean> failResponseCall,
    dynamic paramsData,
    Map<String, dynamic> queryParam,
    Map<String, dynamic> header,
    Options option,
    CancelToken cancelToken,
    OnErrorTipCall onErrorTipCall,
    OnLoadingTipCall onLoadingTipCall,
    bool showLoadingEnable,
    void suc(T tBean),
    void fai(HttpApiBean resBean),
  }) async {
    HttpApiBean<dynamic> _apiBean = await _request(
      context,
      interface,
      method: method,
      sucResponseCall: sucResponseCall,
      failResponseCall: failResponseCall,
      paramsData: paramsData,
      queryParam: queryParam,
      header: header,
      option: option,
      cancelToken: cancelToken,
      onErrorTipCall: onErrorTipCall,
      onLoadingTipCall: onLoadingTipCall,
      showLoadingEnable: showLoadingEnable,
    );

    var _tApiBean = HttpApiBean<T>(
      code: _apiBean.code,
      success: _apiBean.success,
      type: _apiBean.type,
      errorMsg: _apiBean.errorMsg,
      detailError: _apiBean.detailError,
    );

    ///数据处理
    if (_apiBean?.code == httpStateSuc) {
      var _cvData = cvApiBean2Data?.call(_apiBean) ?? _apiBean?.data;
      T _bean = _cvData != null && _cvData is Map
          ? conversionEachBean?.call(_cvData as Map<String, dynamic>)
          : null;
      suc?.call(_bean);
      _tApiBean.data = _bean;
    } else {
      fai?.call(_apiBean);
    }

    return _tApiBean;
  }

  ///发起网络请求
  ///[ interface] 请求接口
  ///[ method] 请求方法
  ///[ contentType] 请求内容体类型
  ///[ paramsData] 请求内容体数据
  ///[ header] 请求头
  ///[ option] 配置
  ///[ showTip] 是否显示tip
  ///[ cancelToken] 关闭请求token
  Future<HttpApiBean<List<T>>> requestBeanList<T>(
    BuildContext context,
    String interface, {
    @required BsHttpMethod method,
    @required ConversionEachBean<T> conversionEachBean,
    List cvApiBean2Data(HttpApiBean apiBean),
    OnResponse2BeanSucCall<HttpApiBean> sucResponseCall,
    OnResponse2BeanFailCall<HttpApiBean> failResponseCall,
    dynamic paramsData,
    Map<String, dynamic> queryParam,
    Map<String, dynamic> header,
    Options option,
    CancelToken cancelToken,
    OnErrorTipCall onErrorTipCall,
    OnLoadingTipCall onLoadingTipCall,
    bool showLoadingEnable,
    void suc(List<T> tBean),
    void fai(HttpApiBean resBean),
  }) async {
    HttpApiBean<dynamic> _apiBean = await _request(
      context,
      interface,
      method: method,
      sucResponseCall: sucResponseCall,
      failResponseCall: failResponseCall,
      paramsData: paramsData,
      queryParam: queryParam,
      header: header,
      option: option,
      cancelToken: cancelToken,
      onErrorTipCall: onErrorTipCall,
      onLoadingTipCall: onLoadingTipCall,
      showLoadingEnable: showLoadingEnable,
    );

    var _tApiBean = HttpApiBean<List<T>>(
      code: _apiBean.code,
      success: _apiBean.success,
      type: _apiBean.type,
      errorMsg: _apiBean.errorMsg,
      detailError: _apiBean.detailError,
    );

    ///数据处理
    if (_apiBean?.code == httpStateSuc) {
      var _cvData = cvApiBean2Data?.call(_apiBean) ?? _apiBean?.data;
      List<T> _beanList = _cvData != null && _cvData is List
          ? _cvData
              ?.map((e) => e == null
                  ? null
                  : conversionEachBean?.call(e as Map<String, dynamic>))
              ?.toList()
          : null;
      suc?.call(_beanList);
      _tApiBean.data = _beanList;
    } else {
      fai?.call(_apiBean);
    }

    return _tApiBean;
  }

  ///网络请求成功状态
  static const int httpStateSuc = 200;

  ///用户token-key
  static const authTokenKey = 'ESL_TOKEN';

  ///用户token
  String _authToken;

  void updateAuthToken(String authToken) {
    this._authToken = authToken;
  }

  String get queryAuthToken {
    return _authToken;
  }
}

///替换成实际情况的数据
class HttpApiBean<T> {
  int code;
  bool success;
  int type;
  String errorMsg;
  String detailError;
  T data;

  HttpApiBean({
    this.code,
    this.success,
    this.type,
    this.errorMsg,
    this.detailError,
    this.data,
  });

  static const String code_ = 'code';
  static const String success_ = 'success';
  static const String type_ = 'type';
  static const String errorMsg_ = 'errorMsg';
  static const String detailError_ = 'detailError';
  static const String data_ = 'data';
}
