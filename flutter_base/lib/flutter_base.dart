library flutter_base;

export 'package:dio/dio.dart';
export 'package:connectivity/connectivity.dart';
export 'package:flutter_base/bean/VMBean.dart';
export 'package:flutter_base/helper/bs_text_input_formatter.dart';
export 'package:flutter_base/helper/chinese_cupertino_localizations.dart';
export 'package:flutter_base/http/bs_http_mixin.dart';
export 'package:flutter_base/view/bs_view.dart';
export 'package:flutter_base/view/text/bs_expandable_text.dart';
export 'package:flutter_base/view/state/bs_many_state_view.dart';
export 'package:flutter_base/view/list/bs_smart_list_view.dart';
export 'package:flutter_base/view/input/bs_bottom_edit_text_view.dart';
export 'package:flutter_base/view/app_bar/bs_app_bar_view.dart';
export 'package:flutter_base/manager/bs_connectivity_manager.dart';
export 'package:flutter_base/manager/bs_download_file_manager.dart';
export 'package:flutter_base/manager/bs_event_bus_manager.dart';
export 'package:flutter_base/manager/bs_clear_cache_manager.dart';
export 'package:flutter_base/route/animation_page_route.dart';

/// A Calculator.
class CalculatorTest {
  /// Returns [value] plus 1.
  int addOne(int value) => value + 14;
}
