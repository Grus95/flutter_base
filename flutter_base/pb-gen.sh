#!/bin/bash

TAGET_ROOT_DIR=""

ROOTDIR=$(
  $( cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)
  pwd -P
)

if [ -n "$TAGET_ROOT_DIR"  ]; then
  ROOTDIR=$TAGET_ROOT_DIR
fi

PROTO_DIR="$ROOTDIR/proto"
DART_OUT="$ROOTDIR/lib/generated/proto/"

function build_proto() {
  local proto_path=$PROTO_DIR
  local proto_file=$1
  local dart_out=$DART_OUT

  if [ ! -e "${dart_out}" ]; then
    mkdir -p "${dart_out}"
  fi

  protoc --proto_path="$proto_path" --dart_out="$dart_out" "$name"
}

function find_and_build() {
  local folder=${ROOTDIR}
  for name in $(find ${folder} -name "*.proto"); do
    echo "build file: $name"
    build_proto $name
  done
}

function clear_old_build_file() {
  local old_build_dir=$DART_OUT
  rm -r $old_build_dir
}

function check_proto_cmd() {
  if [ ! $(which protoc) ]; then
    echo "[ERROR]: not find protoc"
    echo "please install protoc "
    exit -1
  fi

  if [ ! $(which protoc-gen-dart) ]; then
    echo "[ERROR]: not find protoc-gen-dart"
    echo "please use pub global activate protoc_plugin "
    exit -1
  fi
}

check_proto_cmd

echo "======================== Begin ${0}"

echo "clear old build file Bengin"
clear_old_build_file
echo "clear old build file End"
find_and_build

echo "======================== End   ${0}"
